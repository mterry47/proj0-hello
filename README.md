# Proj0-Hello
-------------

Author: Maxwell H Terry
Contact: mterry7@uoregon.edu

Trivial project to exercise version control, turn-in, and other
mechanisms. Project will print "Hello world" when run by pulling a
message out of the credentials.ini file.